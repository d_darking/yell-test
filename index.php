<?php
namespace yell_controller;

use yell\graphics\application\ImageFormatController;

/**
 * PSR-0 autoload
 */
spl_autoload_register(function ($className) {
    $className = ltrim($className, '\\');
    $fileName = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require 'classes' . DIRECTORY_SEPARATOR . $fileName;
});

// ��� ������
$_POST['shapes'] = [
    [
        'type' => 'circle',
        'params' => [
            'x' => 100, 'y' => 100, 'radius' => 50,
            'color' => [
                'r' => 255,
                'g' => 0,
                'b' => 0
            ]
        ]
    ],
    [
        'type' => 'circle',
        'params' => [
            'x' => 200, 'y' => 200, 'radius' => 100,
            'color' => [
                'r' => 0,
                'g' => 255,
                'b' => 0
            ]
        ]
    ],
    [
        'type' => 'square',
        'params' => [
            'x' => 300, 'y' => 300, 'size' => 50,
            'color' => [
                'r' => 0,
                'g' => 0,
                'b' => 255
            ]
        ]
    ],
];

$controller = new ImageFormatController();
$controller->process();


