<?php
namespace yell\graphics\shapes;

/**
 * ������� �������� ����� � ����������� �� ����������� ����
 * Class ShapeFactory
 * @package yell\graphics\shapes
 */
class ShapeFactory
{
    public static function getInstance($type, $params = [])
    {
        $shapeClass = 'yell\graphics\shapes\Shape' . ucfirst($type);
        if (!class_exists($shapeClass)) {
            throw new \UnexpectedValueException('Shape class ' . $shapeClass . ' is not found');
        }
        return new $shapeClass($params);
    }
}