<?php
namespace yell\graphics\shapes;

class ShapeSquare extends Shape
{
    public function getArea()
    {
        $size = $this->getProperty('size');
        return $size * $size;
    }

    public function getSize()
    {
        return $this->getProperty('size');
    }
}