<?php
namespace yell\graphics\shapes;

class ShapeCircle extends Shape
{
    public function getRadius()
    {
        return $this->getProperty('radius');
    }
}