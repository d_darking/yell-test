<?php

namespace yell\graphics\shapes;

/**
 * ������. ����� ������ ���.
 * Class Shape
 * @package yell\graphics\shapes
 */
class Shape
{
    protected $settings = [
        'lineWidth' => 1,
        'lineColor' => '#000000',
    ];

    /**
     * �����������. �������������� ��� �������� ����� ���������� �����. ��� �������� �� ��������� ��������� �������, � ������ �� � ������������� �������. ����� ���������.
     * @param $settings
     */
    public function __construct($settings)
    {
        // ����� ����� ��������� �������������� ��������� �������
        $this->settings = array_merge_recursive($this->settings, $settings);
    }

    public function getProperty($name)
    {
        if (isset($this->settings[$name])) {
            return $this->settings[$name];
        } else {
            throw new \RuntimeException('Unknown property');
        }
    }

    public function getCoordinates()
    {
        return [
            'x' => $this->getProperty('x'),
            'y' => $this->getProperty('y')
        ];
    }
}