<?php

namespace yell\graphics\application;

use yell\graphics\formatters\FormatterImage;
use yell\graphics\formatters\FormatterXml;
use yell\graphics\shapes\ShapeFactory;

class ImageFormatController
{
    public function process()
    {
        $request = Request::getInstance();
        $shapes = $request->getPost('shapes');
        $format = $request->getQueryParam('format', 'image');

        switch ($format) {
            case 'xml':
                $formatter = new FormatterXml();
                break;
            case 'image':
            default:
                $formatter = new FormatterImage(640, 480);
                break;
        }

        if (!empty($shapes)) foreach ($shapes as $shape) {
            if (empty($shape['type'])) {
                throw new \RuntimeException('Bad shapes format. Empty type');
            }
            $shapeInstance = ShapeFactory::getInstance($shape['type'], $shape['params']);
            $formatter->processShape($shapeInstance);
        }
        $formatter->output();

    }
}