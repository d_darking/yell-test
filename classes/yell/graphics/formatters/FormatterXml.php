<?php
namespace yell\graphics\formatters;

use yell\graphics\shapes\ShapeCircle;
use yell\graphics\shapes\ShapeSquare;

/**
 * ������������ XML �� ������ �����. ������ ��� ������, �������� ��� ��������� SVG.
 * Class FormatterXml
 * @package yell\graphics\formatters
 */
class FormatterXml extends Formatter
{
    private $document = '';

    public function __construct()
    {

        $this->document = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $this->document .= '<shapes>';
    }

    public function output()
    {
        header("Content-Type: text/xml");
        $this->document .= '</shapes>';
        echo $this->document;
    }

    protected function processShapeCircle(ShapeCircle $shape)
    {
        $this->document .= '<circle>circle data here</circle>';
    }

    protected function processShapeSquare(ShapeSquare $shape)
    {
        $this->document .= '<square>square data here</square>';
    }
}