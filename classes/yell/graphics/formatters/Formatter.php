<?php
namespace yell\graphics\formatters;

use yell\graphics\shapes\Shape;
use yell\graphics\shapes\ShapeCircle;
use yell\graphics\shapes\ShapeSquare;

/**
 * ����������� ����� �������������� ������ ������.
 * Class Formatter
 * @package yell\graphics\formatters
 */
abstract class Formatter
{
    /**
     * �������������� ������ - ����� ���������������� ������
     * @param Shape $shape
     */
    public function processShape(Shape $shape)
    {
        $class = (new \ReflectionClass($shape))->getShortName();
        $fn = 'process' . $class;
        if (method_exists($this, $fn)) {
            $this->{$fn}($shape);
        } else {
            throw new \UnexpectedValueException('Figure process is not supported for ' . $class);
        }
    }

    abstract public function output();

    abstract protected function processShapeCircle(ShapeCircle $shape);

    abstract protected function processShapeSquare(ShapeSquare $shape);
}