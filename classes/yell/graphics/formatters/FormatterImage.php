<?php
namespace yell\graphics\formatters;

use yell\graphics\shapes\ShapeCircle;
use yell\graphics\shapes\ShapeSquare;

/**
 * ������������ �������� �� ������ �����
 * Class FormatterImage
 * @package yell\graphics\formatters
 */
class FormatterImage extends Formatter
{
    private $image;

    public function __construct($width = 640, $height = 480)
    {
        $this->image = imagecreatetruecolor($width, $height);

        if (!$this->image) {
            throw new \RuntimeException('Cant create image. May be GD library is not installed');
        }
    }

    public function output()
    {
        header("Content-type: image/jpeg");
        imagejpeg($this->image);
    }

    protected function processShapeCircle(ShapeCircle $shape)
    {
        $coordinates = $shape->getCoordinates();
        $color = $shape->getProperty('color');
        $color = imagecolorallocate($this->image, $color['r'], $color['g'], $color['b']);
        imagefilledellipse($this->image, $coordinates['x'], $coordinates['y'], $shape->getRadius(), $shape->getRadius(), $color);
    }

    protected function processShapeSquare(ShapeSquare $shape)
    {
        // function imagefilledrectangle ($image, $x1, $y1, $x2, $y2, $color) {}
        $coordinates = $shape->getCoordinates();
        $color = $shape->getProperty('color');
        $color = imagecolorallocate($this->image, $color['r'], $color['g'], $color['b']);
        imagefilledrectangle($this->image,
            $coordinates['x'],
            $coordinates['y'],
            $coordinates['x'] + $shape->getProperty('size'),
            $coordinates['y'] + $shape->getProperty('size'),
            $color);
    }
}